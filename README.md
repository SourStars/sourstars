# SourStars Releases

There are two ways to get SourStars.

## From Docker Image

```shell
# port 12345 for sourstars server, and 23456 for sourstars frontend
docker run -it -p 12345:12345 -p 23456:23456 sourstars/sourstars:master
```

Or turn to [GitLab Container Registry](https://gitlab.com/SourStars/sourstars/container_registry) or [Docker Hub](https://hub.docker.com/r/sourstars/sourstars) to find more tags.

## From Release Archive

Get latest release link as `$LINK` from [releases](https://gitlab.com/SourStars/sourstars/-/releases).

Notice: `git`, `cmake`, `make` and `g++` (or `clang++` with stdlib) are required in your environment.

### HOW TO GET
```shell
wget $LINK -O sourstars.zip
```

### HOW TO INIT

```shell
unzip sourstars.zip
./sourstars/register-sourstars
```

## Usage

### VIA CLI

```shell
sourstars-scan $SOURSTARS_PATH/sourstars-scan/example_config.json 
```

### VIA WEB
```shell
# in your server
sourstars-server-with-frontend > ~/sourstars-server.log 2>&1 &

# in your client, with $YOUR_SERVER_HOSTNAME as hostname of your server
firefox http://$YOUR_SERVER_HOSTNAME:23456 # or other browser whatever you like
```
